var dependencies = {
    madjoh_modules : [
        'styling',
        'common_images'
    ]
};
var list = MadJohRequire.getList(dependencies);
define(list, function(require, Styling, CommonImages){
	var styles = ['madjoh_form'];
    MadJohRequire.getCSS(styles);

    element = document.querySelectorAll('.madjohForm ul li, .homeConnectionButtons li');
    for(var i=0; i<element.length; i++){
        element[i].style.marginTop = Styling.getHeight()*0.015+'px';
    }

    element = document.querySelectorAll('.madjohForm ul li input');
    for(var i=0; i<element.length; i++){
        element[i].style.height = Styling.getHeight()*0.07+'px';
        element[i].style.fontSize = Styling.getHeight()*0.03+'px';
    }

    element = document.querySelectorAll('.madjohForm ul li.madjohFormButton, .homeConnectionButtons li.madjohFormButton');
    for(var i=0; i<element.length; i++){
        element[i].style.height = Styling.getHeight()*0.07+'px';
    }

    element = document.querySelectorAll('.madjohForm ul li.madjohFormButton span, .homeConnectionButtons li.madjohFormButton span');
    for(var i=0; i<element.length; i++){
        element[i].style.fontSize = Styling.getHeight()*0.04+'px';
        element[i].style.lineHeight = Styling.getHeight()*0.07+'px';
        element[i].style.left = Styling.getHeight()*0.03+'px';
    }
});